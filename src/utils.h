#ifndef UTILS_H
#define UTILS_H

/// @file utils.h
/// @brief Utility functions for our examples
/// @author Quentin Barbarat

/// @brief Says hello to the user on stdout
///
void hello(void);

#endif /* ! UTILS_H */
